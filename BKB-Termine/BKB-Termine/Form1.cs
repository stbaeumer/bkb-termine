﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BKB_Termine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ConvertJsonToDataTable();
        }

        private void dgvTermine_RowEnter(object sender,
    DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < dgvTermine.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvTermine[i, e.RowIndex].Style.BackColor = Color.Yellow;
            }
        }

        private void dgvTermine_RowLeave(object sender,
            DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < dgvTermine.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvTermine[i, e.RowIndex].Style.BackColor = Color.Empty;
            }
        }

        private void ConvertJsonToDataTable()
        {
            try
            {
                string jsonString = File.ReadAllText(@"U:\Source\Repos\json-termine\termine.json");

                if (!String.IsNullOrWhiteSpace(jsonString))
                {
                    dynamic termine = JsonConvert.DeserializeObject(jsonString);
                    
                    DataTable dtTermine = new DataTable();

                    dtTermine.Columns.Add("tag", typeof(string));
                    dtTermine.Columns.Add("datum", typeof(string));
                    dtTermine.Columns.Add("von-bis", typeof(string));
                    dtTermine.Columns.Add("inhalt", typeof(string));
                    dtTermine.Columns.Add("jahrgang", typeof(string));
                    dtTermine.Columns.Add("beginn", typeof(string));
                    dtTermine.Columns.Add("ende", typeof(string));
                    dtTermine.Columns.Add("raum", typeof(string));
                    dtTermine.Columns.Add("verantwortich", typeof(string));
                    dtTermine.Columns.Add("kategorie", typeof(string));
                    dtTermine.Columns.Add("Hinweise", typeof(string));
                    dtTermine.Columns.Add("Sortierung", typeof(string));
                    
                    foreach (var termin in termine)
                    {
                        string cou1 = Convert.ToString(termin);
                        string[] RowData = Regex.Split(cou1.Replace
                        ("{", "").Replace("}", ""), ",");
                        DataRow nr = dtTermine.NewRow();
                        foreach (string rowData in RowData)
                        {
                            try
                            {
                                int idx = rowData.IndexOf(":");
                                string RowColumns = rowData.Substring
                                (0, idx - 1).Replace("\"", "").Trim();
                                string RowDataString = rowData.Substring
                                (idx + 1).Replace("\"", "");
                                nr[RowColumns] = RowDataString;
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                        dtTermine.Rows.Add(nr);
                    }

                    foreach (var off in termine)
                    {
                        string off1 = Convert.ToString(off);
                        string[] RowData = Regex.Split(off1.Replace
                        ("{", "").Replace("}", ""), ",");
                        DataRow nr = dtTermine.NewRow();
                        foreach (string rowData in RowData)
                        {
                            try
                            {
                                int idx = rowData.IndexOf(":");
                                string RowColumns = rowData.Substring
                                (0, idx - 1).Replace("\"", "").Trim();
                                string RowDataString = rowData.Substring
                                (idx + 1).Replace("\"", "");
                                nr[RowColumns] = RowDataString;
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                        dtTermine.Rows.Add(nr);
                    }

                    if (dtTermine.Rows.Count > 0)
                    {
                        dgvTermine.DataSource = dtTermine;
                        //dgvTermine.Columns["id"].Visible = false;
                        dgvTermine.Columns["tag"].HeaderText = "Tag";
                        dgvTermine.Columns["datum"].HeaderText = "Datum";
                        dgvTermine.Columns["von-bis"].HeaderText = "von-bis";
                        dgvTermine.Columns["inhalt"].HeaderText = "Inhalt";
                        dgvTermine.Columns["jahrgang"].HeaderText = "Jahrgang";
                        dgvTermine.Columns["beginn"].HeaderText = "Beginn";
                        dgvTermine.Columns["ende"].HeaderText = "Ende";
                        dgvTermine.Columns["raum"].HeaderText = "Raum";
                        dgvTermine.Columns["verantwortlich"].HeaderText = "Verantwortlich";
                        dgvTermine.Columns["kategorie"].HeaderText = "Kategorie";
                        dgvTermine.Columns["hinweise"].HeaderText = "Hinweise";
                        dgvTermine.Columns["sortierung"].Visible = false;                        
                        dgvTermine.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR",
        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
